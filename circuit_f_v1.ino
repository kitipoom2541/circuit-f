//#include <BLEDevice.h>
//#include <BLEUtils.h>
//#include <BLEServer.h>
//#include <BLE2902.h>


#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define  SSD1306_WHITE 1
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

int IR_L   = 16; //GPIO16
int G_L    = 17; //GPIO17
int IR_R   = 35; //IO35
int G_R    = 39; //IO39
int BP_sig = 34; //IO34
int btn    = 33; //IO33

int p_rate     = 0;
int t_value    = 0;
int maximum_v  = 0;
int min_bp     = 0;
int new_v      = 0;
int pulse_up   = 0;
float f_time   = 0;
int up         = 0;
float down     = 0;
int new_a      = 0;
float up_a     = 0;
float down_a   = 0;
int g_value    = 0;
int m_green    = 0;
int m_green_1  = 0;
int m_green_2  = 0;
int m_green_3  = 0;
int m_green_4  = 0;
int m_green_5  = 0;
float sum_g      = 0;
int ir_value   = 0;
int m_ir       = 0;
int m_ir_1     = 0;
int m_ir_2     = 0;
int m_ir_3     = 0;
int m_ir_4     = 0;
int m_ir_5     = 0;
float sum_ir     = 0;
int g_status   = 0;
int ir_status  = 0;
int max_bp     = 0;
int sum_bp     = 0;
int Sys        = 0;
int Di         = 0;
int Mean       = 0;
int b_glucose  = 0;
int blood_p    = 0;
int sum_min_bp = 0;
int sum_max_bp = 0;
int  t         = 0;
int  pulse     = 0;
int  c_pulse   = 0;
float pulse_oxi  = 0;

int theshold = 0;
int bmax = 0;
int bnew = 0;
int B = 0;
int x = 0;
//int up = 0;
//int down = 0;
int Status = 0;
float T = 0;
int Time = 0;

unsigned long start_time;
unsigned long end_time;

unsigned long initial_pulse_time;
unsigned long final_pulse_time;




void setup() {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();

  pinMode(IR_L, OUTPUT);
  pinMode(G_L,  OUTPUT);
  pinMode(IR_R, INPUT);
  pinMode(G_R,  INPUT);
  pinMode(BP_sig, INPUT);
  pinMode(btn, INPUT);

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(20, 0);
  display.println("WELCOME");
  display.display();
  delay(1000);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 20);
  display.println("Waiting.");
  display.display();
  delay(1000);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 20);
  display.println("Waiting..");
  display.display();
  delay(1000);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 20);
  display.println("Waiting...");
  display.display();
  delay(1000);

  display.clearDisplay();
}

void loop() {

  //  digitalWrite(IR_L, HIGH);
  b_glucose  = 0;
  blood_p    = 0;

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring.");

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(20, 0);
  display.println("  SpO2 ");

///////////////////////////////////////////////////////////////////////

  display.display();
  delay(500);
  digitalWrite(G_L, HIGH);
  for (g_status = 0; g_status < 5; g_status++) {

    for (int i = 0; i < 100; i++) {
      digitalWrite(G_L, HIGH);
      new_v = analogRead(G_R);
      Serial.print("g_value : ");
      Serial.println(new_v);
      if (m_green > new_v) {
        m_green = m_green;
      }
      else if (m_green < new_v) {
        m_green = new_v;
      }
    }


    Serial.print("Sum G : ");
    Serial.println(m_green);
    sum_g = sum_g + m_green;
    digitalWrite(G_L, LOW);
  }
  digitalWrite(G_L, LOW);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring..");
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(20, 0);
  display.println("  SpO2 ");
  display.display();

  digitalWrite(G_L, LOW);

  digitalWrite(IR_L, HIGH);

///////////////////////////////////////////////////////////////////////

  for (ir_status = 0; ir_status < 5; ir_status++) {
    digitalWrite(G_L, LOW);
    digitalWrite(IR_L, HIGH);
    for (int i = 0; i < 100; i++) {
      digitalWrite(IR_L, HIGH);
      new_v = analogRead(IR_R);
      Serial.print("ir_value : ");
      Serial.println(new_v);
      if (m_ir > new_v) {
        m_ir = m_ir;
      }
      else if (m_ir < new_v) {
        m_ir = new_v;
      }
    }

    Serial.print("Sum IR : ");
    Serial.println(m_ir);
    sum_ir = sum_ir + m_ir;
  }
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring...");
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(20, 0);
  display.println("GLUCOSE");
  display.display();
  digitalWrite(G_L, LOW);

///////////////////////////////////////////////////////////////////////

  for (ir_status = 0; ir_status < 5; ir_status++) {
    digitalWrite(G_L, LOW);
    for (int i = 0; i < 200; i++) {
      new_v = analogRead(IR_R);
      Serial.print("ir_value : ");
      Serial.println(new_v);
      if (m_ir > new_v) {
        m_ir = m_ir;
      }
      else if (m_ir < new_v) {
        m_ir = new_v;
      }
    }
    Serial.print("Glu_IR : ");
    Serial.println(m_ir);
    b_glucose = b_glucose + m_ir;
  }

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring....");
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(20, 0);
  display.println("GLUCOSE");
  display.display();

  pulse_oxi = ( (sum_g / 5) / ((sum_g / 5) + (sum_ir / 5)) );

///////////////////////////////////////////////////////////////////////

  for (int t = 0; t < 3; t++) {
    for (int i = 0; i < 150; i++) {
      digitalWrite(G_L, HIGH);
      new_v = analogRead(BP_sig);
      Serial.print("bp_value : ");
      Serial.println(new_v);
      if (max_bp > new_v) {
        max_bp = max_bp;
      }
      else if (max_bp < new_v) {
        max_bp = new_v;
      }
      else if (min_bp > new_v) {
        min_bp = new_v;
      }
      else if (min_bp < new_v) {
        min_bp = min_bp;
      }

    }
    sum_max_bp = sum_max_bp + max_bp;
    sum_min_bp = sum_min_bp + min_bp;
    Serial.print("max_bp : ");
    Serial.println(sum_max_bp / 3);
    Serial.print("min_bp : ");
    Serial.println(sum_min_bp / 3);
  }
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring.");
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(" PRESSURE ");
  display.display();
  digitalWrite(IR_L, HIGH);
  delay(500);
  blood_p = ((sum_max_bp / 3) - (sum_min_bp / 3));

///////////////////////////////////////////////////////////////////////

  digitalWrite(G_L, HIGH);
  theshold = 0;
  Status = 0;
  bmax = 0;
  bnew = 0;
  Time = 0;
  down = 0;
  up = 0;
  B = 0;
  x = 0;
  T = 0;

  for (B = 0; B < 200; B++) {
    bnew = analogRead(G_R);
    Serial.print("bnew");
    Serial.println(bnew);
    delay(10);
    if (bmax > bnew ) {
      bmax = bmax;
    }
    else if (bmax < bnew) {
      bmax = bnew;
    }
  }
  bmax = bmax - 100;
  theshold = bmax;
  Serial.print("Threshold");
  Serial.println(theshold);

  start_time = millis();
  delay(1000);

  while (down != 2 && theshold > 0) {
    delay(15);
    x = analogRead(G_R);
    Serial.print("x");
    Serial.println(x);
    if (Status == 0 && up < 2) {
      if (theshold < x) {
        up++;
        Status = 1;
        Serial.println(x);
      }
    }
    end_time = millis();
    if (end_time - start_time > 8000) {
      break;
    }

    else if (Status == 1 && down < 2) {
      if (theshold > x) {
        down++;
        Status = 0;
        Serial.println(x);
      }
    }
    end_time = millis();
    if (end_time - start_time > 8000) {
      break;
    }
  }
  end_time = millis();

  T = (end_time - start_time);
  Time = 20 + (1 / (T / 60000)) + 40.5;
  Serial.print("PR: = ");
  Serial.println(Time); for (B = 0; B < 200; B++) {
    bnew = analogRead(G_R);
    Serial.print("bnew");
    Serial.println(bnew);
    delay(10);
    if (bmax > bnew ) {
      bmax = bmax;
    }
    else if (bmax < bnew) {
      bmax = bnew;
    }
  }
  bmax = bmax - 100;
  theshold = bmax;
  Serial.print("Threshold");
  Serial.println(theshold);

  start_time = millis();
  delay(1000);

  while (down != 2 && theshold > 0) {
    delay(15);
    x = analogRead(G_R);
    Serial.print("x");
    Serial.println(x);
    if (Status == 0 && up < 2) {
      if (theshold < x) {
        up++;
        Status = 1;
        Serial.println(x);
      }
    }
    end_time = millis();
    if (end_time - start_time > 8000) {
      break;
    }

    else if (Status == 1 && down < 2) {
      if (theshold > x) {
        down++;
        Status = 0;
        Serial.println(x);
      }
    }
    end_time = millis();
    if (end_time - start_time > 8000) {
      break;
    }
  }
  end_time = millis();

  T = (end_time - start_time);
  Time = 20 + (1 / (T / 60000)) + 40.5;
  Serial.print("PR: = ");
  Serial.println(Time);

///////////////////////////////////////////////////////////////////////

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring..");
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(1, 0);
  display.println("PULSE RATE");
  display.display();

  Serial.print("Final_Time : ");
  Serial.println(Time);

  digitalWrite(G_L, HIGH);


  digitalWrite(G_L, LOW);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.println("Measuring...");
  display.display();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(1, 0);
  display.println("PULSE RATE");
  delay(800);


  Serial.print("SpO2 : ");
  Serial.println(pulse_oxi);

  Serial.print("Glucose : ");
  Serial.println(b_glucose / 5);

  Serial.print("BP : ");
  Serial.println(blood_p);

  Serial.print("Pulse rate : ");
  Serial.println(Time);

  Serial.print("Sum of G : ");
  Serial.println(sum_g / 5);

  Serial.print("Sum of IR : ");
  Serial.println(sum_ir / 5);

  display.clearDisplay();
  delay(500);


  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  display.print("SpO2 : ");
  display.println(pulse_oxi);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 20);
  display.print("Glucose : ");
  display.println(b_glucose / 5);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 30);
  display.print("Pulse rate : ");
  display.println(Time);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 40);
  display.print("Sum of G : ");
  display.println(sum_g / 5);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 50);
  display.print("Sum of IR : ");
  display.println(sum_ir / 5);
  display.display();

  delay(10000);

  theshold   = 0;
  bmax       = 0;
  bnew       = 0;
  B          = 0;
  Time       = 0;
  t          = 0;
  pulse      = 0;
  p_rate     = 0;
  c_pulse    = 0;
  p_rate     = 0;
  t_value    = 0;
  maximum_v  = 0;
  min_bp     = 0;
  new_v      = 0;
  pulse_up   = 0;
  f_time     = 0;
  up         = 0;
  down       = 0;
  new_a      = 0;
  up_a       = 0;
  down_a     = 0;
  g_value    = 0;
  m_green    = 0;
  m_green_1  = 0;
  m_green_2  = 0;
  m_green_3  = 0;
  m_green_4  = 0;
  m_green_5  = 0;
  sum_g      = 0;
  ir_value   = 0;
  m_ir       = 0;
  m_ir_1     = 0;
  m_ir_2     = 0;
  m_ir_3     = 0;
  m_ir_4     = 0;
  m_ir_5     = 0;
  sum_ir     = 0;
  g_status   = 0;
  ir_status  = 0;
  max_bp     = 0;
  sum_bp     = 0;
  Sys        = 0;
  Di         = 0;
  Mean       = 0;
  b_glucose  = 0;
  blood_p    = 0;
  sum_min_bp = 0;
  sum_max_bp = 0;
  pulse_oxi  = 0;
  initial_pulse_time = 0;
  final_pulse_time   = 0;
}
